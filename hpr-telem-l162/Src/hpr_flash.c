/*
 * hpr_flash.c
 *
 *  Created on: Apr 18, 2017
 *      Author: ariolis
 */
#include "hpr_flash.h"

HPR_rbf_status HPR_get_rbf_status(void){

	HPR_rbf_status current_rbf_status;
	//set rbf_out high and wait a few ticks.
	HAL_GPIO_WritePin(RBF_OUT_GPIO_Port, RBF_OUT_Pin, GPIO_PIN_SET);
	//HAL_Delay(1);


	current_rbf_status = HPR_RBF_FLIGHT_MODE;

	for (uint8_t var = 0; var < 10; ++var) {

		GPIO_PinState rbf_pin = HAL_GPIO_ReadPin(RBF_IN_GPIO_Port,RBF_IN_Pin);
		//HAL_Delay(1);
		if (rbf_pin == GPIO_PIN_SET) {
			current_rbf_status = HPR_RBF_GROUND_MODE;
			break;
		}
	}

	return current_rbf_status;

}



/**
  * @brief Get word from a specific memory address.
  *
  * @param memory_address: memory address - good idea to pass only defined values and avoid mishapsmemory_address
  * @param memory_data:  32bit word just read from the specified memory address.
  *
  * @retval None.
  */
void HPR_get_memory_word(uint32_t memory_address, uint32_t *memory_data ){

	  uint32_t flash_read_value;
	  flash_read_value = *( (uint32_t *)memory_address);
	  *memory_data  = flash_read_value;

}

/**
  * @brief Set word from a specific memory address.
  *
  * @param memory_address: memory address - good idea to pass only defined values and avoid mishapsmemory_address
  * @param memory_data: payload, 32bit word to write at the specified memory address.
  *
  * @retval None.
  */
void HPR_save_to_flash_memory_word(uint32_t *memory_data){

	uint32_t flash_write_value = *memory_data;

	/*if we are allowed to write to flash - max length not reached*/
	if(flash_memory_write_status){
		/* write to memory  */
		HAL_FLASH_Unlock();
		HAL_FLASH_Program(TYPEPROGRAM_WORD, flash_memory_write_adress, flash_write_value);
		HAL_FLASH_Lock();

		flash_memory_write_adress = flash_memory_write_adress+4;
		if(flash_memory_write_adress>=HPR_FLASH_DATASTORE_STOP_ADRESS){
            //if flash memory is full stop logging data
			flash_memory_write_status = 0x00;
		}
	}
	else{
		//this means that flash is full and it stoped writting.
	}

}
