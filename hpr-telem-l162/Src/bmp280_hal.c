
#include "bmp280_hal.h"
#include "stm32l1xx_hal.h"
#include "cmsis_os.h"

#define	I2C_BUFFER_LEN 8
#define BMP280_DATA_INDEX	1
#define BMP280_ADDRESS_INDEX	2
/*	\Brief: The function is used as I2C bus read
 *	\Return : Status of the I2C read
 *	\param dev_addr : The device address of the sensor
 *	\param reg_addr : Address of the first register, where data is going to be read
 *	\param reg_data : This is the data read from the sensor, which is held in an array
 *	\param cnt : The no of bytes of data to be read
 */
s8 BMP280_I2C_bus_read(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt);
 /*	\Brief: The function is used as I2C bus write
 *	\Return : Status of the I2C write
 *	\param dev_addr : The device address of the sensor
 *	\param reg_addr : Address of the first register, where data is to be written
 *	\param reg_data : It is a value held in the array,
 *		which is written in the register
 *	\param cnt : The no of bytes of data to be written
 */
s8 BMP280_I2C_bus_write(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt);

/*	Brief : The delay routine
 *	\param : delay in ms
*/
void BMP280_delay_msek(u32 msek);

struct bmp280_t bmp280;
extern I2C_HandleTypeDef hi2c2;

s32 bmp280_data_read(u32 *pressure, s32 *temperature)
{
	s32 com_rslt = BMP_ERROR;
	bmp280.bus_write = BMP280_I2C_bus_write;
	bmp280.bus_read = BMP280_I2C_bus_read;
	bmp280.dev_addr = BMP280_I2C_ADDRESS1;
	bmp280.delay_msec = BMP280_delay_msek;
	com_rslt = bmp280_init(&bmp280);

	com_rslt += bmp280_set_power_mode(BMP280_NORMAL_MODE);
	com_rslt += bmp280_set_work_mode(BMP280_STANDARD_RESOLUTION_MODE);
	com_rslt += bmp280_set_standby_durn(BMP280_STANDBY_TIME_63_MS);
	com_rslt += bmp280_read_pressure_temperature(pressure, temperature);

	com_rslt += bmp280_set_power_mode(BMP280_SLEEP_MODE);

   return com_rslt;
}

/*	\Brief: The function is used as I2C bus write
 *	\Return : Status of the I2C write
 *	\param dev_addr : The device address of the sensor
 *	\param reg_addr : Address of the first register, where data is to be written
 *	\param reg_data : It is a value held in the array,
 *		which is written in the register
 *	\param cnt : The no of bytes of data to be written
 */
s8  BMP280_I2C_bus_write(u8 dev_id, u8 reg_addr, u8 *reg_data, u8 cnt)
{
	HAL_StatusTypeDef status = HAL_OK;

	while (HAL_I2C_IsDeviceReady(&hi2c2, (dev_id << 1), 3, 1000) != HAL_OK) {
	}

	status = HAL_I2C_Mem_Write(&hi2c2, (dev_id << 1), reg_addr, I2C_MEMADD_SIZE_8BIT, reg_data, cnt,
			1000);
//	TODO: Find a fix to remove this delay
	HAL_Delay(40);
	if (status != HAL_OK) {
		return 1;
	}
	return 0;
}

 /*	\Brief: The function is used as I2C bus read
 *	\Return : Status of the I2C read
 *	\param dev_addr : The device address of the sensor
 *	\param reg_addr : Address of the first register, where data is going to be read
 *	\param reg_data : This is the data read from the sensor, which is held in an array
 *	\param cnt : The no of data to be read
 */
s8 BMP280_I2C_bus_read(u8 dev_id, u8 reg_addr, u8 *reg_data, u8 cnt) {
	HAL_StatusTypeDef status = HAL_OK;
//	memset(reg_data,0,len);
	while (HAL_I2C_IsDeviceReady(&hi2c2, (uint16_t) (dev_id << 1), 3, 1000)
			!= HAL_OK) {
	}

	status = HAL_I2C_Mem_Read(&hi2c2, (uint8_t) (dev_id << 1),
			(uint8_t) reg_addr, I2C_MEMADD_SIZE_8BIT, reg_data, cnt, 1000);

	if (status != HAL_OK) {
		return 1;
	}
	return 0;
}


/*	Brief : The delay routine
 *	\param : delay in ms
*/
void  BMP280_delay_msek(u32 msek)
{
	osDelay(msek);
}
