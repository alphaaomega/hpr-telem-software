/*
 * hpr_ring_buffer.c
 *
 *  Created on: Apr 10, 2017
 *      Author: ariolis
 */
#include "hpr_ring_buffer.h"
#include "hpr_flash.h"
#include "string.h"


void HPR_RingBufferInit(HPR_RingBufferTypeDef *ring_buffer){

	/*Initialize ring buffer index*/
	ring_buffer->index =0;

	/*Initialize with pseudo data this ring buffer */

	for (int entry_index = 0; entry_index < RINGBUFFER_LENGTH; ++entry_index) {

		/*Initialize imu structure log buffer*/
		for (int imu_log_index = 0; imu_log_index < IMU_BUFFER_LENGTH; ++imu_log_index) {
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].magnetometer_x_axis_raw = HPR_DUMMY_DATA_16;
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].magnetometer_y_axis_raw = HPR_DUMMY_DATA_16;
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].magnetometer_z_axis_raw = HPR_DUMMY_DATA_16;
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].gyroscope_x_axis_raw = HPR_DUMMY_DATA_16;
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].gyroscope_y_axis_raw = HPR_DUMMY_DATA_16;
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].gyroscope_z_axis_raw = HPR_DUMMY_DATA_16;
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].accelerometer_x_axis_raw = HPR_DUMMY_DATA_16;
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].accelerometer_y_axis_raw = HPR_DUMMY_DATA_16;
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].accelerometer_z_axis_raw = HPR_DUMMY_DATA_16;
			ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].temperature_raw = HPR_DUMMY_DATA_16;
		}

		/*Initialize gps structure*/
		ring_buffer->data_log[entry_index].gps_datastore.lat;//32bit
		ring_buffer->data_log[entry_index].gps_datastore.lon;//32bit
		ring_buffer->data_log[entry_index].gps_datastore.alt = HPR_DUMMY_DATA_16;
		ring_buffer->data_log[entry_index].gps_datastore.d3fix = HPR_DUMMY_DATA_8;
		ring_buffer->data_log[entry_index].gps_datastore.fix_timestamp = HPR_DUMMY_DATA_16;

		/*Initialize bme structure*/
		ring_buffer->data_log[entry_index].bme_datastore.temperature = HPR_DUMMY_DATA_16;
		ring_buffer->data_log[entry_index].bme_datastore.humidity = HPR_DUMMY_DATA_16;
		ring_buffer->data_log[entry_index].bme_datastore.pressure = HPR_DUMMY_DATA_16;
//		ring_buffer->data_log[entry_index].bme_datastore.value4 = HPR_DUMMY_DATA_16;

		/*Initialize time ticks counter*/
		ring_buffer->data_log[entry_index].time_ticks = 0;

		/*Initialize battery voltage monitor*/
		ring_buffer->data_log[entry_index].battery_voltage = 0;
	}


}


void HPR_RingBufferAddEntry(HPR_RingBufferTypeDef *ring_buffer, IMU_DataStoreTypeDef *imu_data_now, GPS_DataStoreTypeDef *gps_data_now, BME_DataStoreTypeDef *bme_data_now, uint8_t battery_voltage_now ,uint32_t timeticks_now ){

	/*Add Entry to Ring buffer*/

	/*add fresh data to imu buffer*/
	for (int imu_log_index = 0; imu_log_index < IMU_BUFFER_LENGTH; ++imu_log_index) {

		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].magnetometer_x_axis_raw = imu_data_now[imu_log_index].magnetometer_x_axis_raw;
		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].magnetometer_y_axis_raw = imu_data_now[imu_log_index].magnetometer_y_axis_raw;
		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].magnetometer_z_axis_raw = imu_data_now[imu_log_index].magnetometer_z_axis_raw;
		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].gyroscope_x_axis_raw = imu_data_now[imu_log_index].gyroscope_x_axis_raw;
		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].gyroscope_y_axis_raw = imu_data_now[imu_log_index].gyroscope_y_axis_raw;
		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].gyroscope_z_axis_raw = imu_data_now[imu_log_index].gyroscope_z_axis_raw;
		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].accelerometer_x_axis_raw = imu_data_now[imu_log_index].accelerometer_x_axis_raw;
		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].accelerometer_y_axis_raw = imu_data_now[imu_log_index].accelerometer_y_axis_raw;
		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].accelerometer_z_axis_raw = imu_data_now[imu_log_index].accelerometer_z_axis_raw;
		ring_buffer->data_log[ring_buffer->index].imu_datastore[imu_log_index].temperature_raw = imu_data_now[imu_log_index].temperature_raw;
	}

	/*add fresh data to gps buffer*/
	ring_buffer->data_log[ring_buffer->index].gps_datastore.lat = gps_data_now->lat;
	ring_buffer->data_log[ring_buffer->index].gps_datastore.lon = gps_data_now->lon;
	ring_buffer->data_log[ring_buffer->index].gps_datastore.alt = gps_data_now->alt;
	ring_buffer->data_log[ring_buffer->index].gps_datastore.d3fix = gps_data_now->d3fix;
	ring_buffer->data_log[ring_buffer->index].gps_datastore.fix_timestamp = gps_data_now->fix_timestamp;

	/*add fresh data to bme buffer*/
	ring_buffer->data_log[ring_buffer->index].bme_datastore.temperature = bme_data_now->temperature;
	ring_buffer->data_log[ring_buffer->index].bme_datastore.humidity = bme_data_now->humidity;
	ring_buffer->data_log[ring_buffer->index].bme_datastore.pressure = bme_data_now->pressure;
//	ring_buffer->data_log[ring_buffer->index].bme_datastore.value4 = bme_data_now->value4;

	/*add current time ticks stamp*/
	ring_buffer->data_log[ring_buffer->index].time_ticks = timeticks_now;

	/*add current time ticks stamp*/
	ring_buffer->data_log[ring_buffer->index].battery_voltage = battery_voltage_now;

	/*Move index frwrd (check for wrapping)*/
	uint8_t new_ring_buffer_index = ring_buffer->index + 1;
	if(new_ring_buffer_index>=RINGBUFFER_LENGTH){
		new_ring_buffer_index = 0;
	}
	ring_buffer->index = new_ring_buffer_index;


	//maybe it should return a status if buffer has wrapped...?
}


uint8_t HPR_RingBufferGetIndex(HPR_RingBufferTypeDef *ring_buffer){

	return ring_buffer->index;
}

void HPR_RingBufferSaveToSd(HPR_RingBufferTypeDef *ring_buffer){

}



uint8_t HPR_RingBufferIsFull(HPR_RingBufferTypeDef *ring_buffer){

	/*return non zero if full, zero otherwise*/

	//ringbuffer is full when the index is at zero.
	//That means that when it wraps around we consider the buffer full.
	//It also means that if for any reason the ring buffer is not advanced it will keep transmitting/logging stuff...

	if(ring_buffer->index ==0){
		return 0xff;
	}
	return 0;
}

void HPR_RingBufferSaveToFlash(HPR_RingBufferTypeDef *ring_buffer){

	if(HPR_get_rbf_status()==HPR_RBF_FLIGHT_MODE){
		//dump ring buffer contents to flash
		uint8_t copy_buffer[4];
		uint32_t *copy_buffer_index = (uint32_t *)copy_buffer;



		for (int entry_index = 0; entry_index < RINGBUFFER_LENGTH; ++entry_index) {

			for (int imu_log_index = 0; imu_log_index < IMU_BUFFER_LENGTH; ++imu_log_index) {
				/*data logged every 10 msec*/

				//ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].magnetometer_x_axis_raw;//16bit
				//ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].magnetometer_y_axis_raw;//16bit
				//ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].magnetometer_z_axis_raw;//16bit
//				memcpy(copy_buffer+0, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].gyroscope_x_axis_raw), 2);
//				memcpy(copy_buffer+2, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].gyroscope_y_axis_raw), 2);
//				HPR_save_to_flash_memory_word( copy_buffer_index);
//
//				memcpy(copy_buffer+0, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].gyroscope_z_axis_raw), 2);
//				memcpy(copy_buffer+2, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].accelerometer_x_axis_raw), 2);
//				HPR_save_to_flash_memory_word( copy_buffer_index);
//
//				memcpy(copy_buffer+0, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].accelerometer_y_axis_raw), 2);
//				memcpy(copy_buffer+2, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].accelerometer_z_axis_raw), 2);
//				HPR_save_to_flash_memory_word( copy_buffer_index);


				//ring_buffer->data_log[entry_index].imu_datastore[imu_log_index].temperature_raw;//16bit

			}
            /*data logged every 100 msec*/

//			memcpy(copy_buffer+0, &(ring_buffer->data_log[entry_index].bme_datastore.temperature), 4);
//			HPR_save_to_flash_memory_word( copy_buffer_index);
//
//
//
//			memcpy(copy_buffer+0, &(ring_buffer->data_log[entry_index].bme_datastore.pressure), 4);
//			HPR_save_to_flash_memory_word( copy_buffer_index);
//

		}
		/*data logged every 1000 msec*/

		/*  gps structure*/
		memcpy(copy_buffer+0, &(ring_buffer->data_log[IMU_BUFFER_LENGTH].gps_datastore.lat), 4);
		HPR_save_to_flash_memory_word( copy_buffer_index);

		memcpy(copy_buffer+0, &(ring_buffer->data_log[IMU_BUFFER_LENGTH].gps_datastore.lon), 4);
		HPR_save_to_flash_memory_word( copy_buffer_index);


		/*  time ticks counter*/

		memcpy(copy_buffer+0, &(ring_buffer->data_log[IMU_BUFFER_LENGTH].time_ticks), 4);
		HPR_save_to_flash_memory_word( copy_buffer_index);



		memcpy(copy_buffer+1, &(ring_buffer->data_log[IMU_BUFFER_LENGTH].battery_voltage), 1);
		memcpy(copy_buffer+1, &(ring_buffer->data_log[IMU_BUFFER_LENGTH].gps_datastore.d3fix), 1);
		memcpy(copy_buffer+2, &(ring_buffer->data_log[IMU_BUFFER_LENGTH].gps_datastore.alt), 2);
		HPR_save_to_flash_memory_word( copy_buffer_index);


	}



	else{
		//do not touch flash memory!
	}




}
