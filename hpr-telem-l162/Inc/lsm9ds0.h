/*
 * lsm9ds0.h
 *
 *  Created on: Apr 3, 2017
 *      Author: drid
 */

#ifndef LSM9DS0_H_
#define LSM9DS0_H_

#include <stdint.h>

#define SENSORS_GRAVITY_EARTH (1.0F)

/* IMU LSM9DS0 */
#define GYRO_ADDR       0x6B
#define GYRO_VAL        0x28
#define CTRL_REG1_G     0x20
#define WHO_AM_I_G      0x0F
#define GYRO_ID         0xD4
#define GYRO_GAIN       8.75 / 1e3	// DPS

#define XM_ADDR         0x1D
#define XM_CTR_REG      0x1F
#define WHO_AM_I_XM     0x0F
#define XM_ID           0x49
#define XM_VAL          0x08
#define XM_GAIN         8           // nT/LSB

// Temperature
#define TEMP_ADDR		0x1D
#define CTRL_REG5_XM	0x24
#define OUT_TEMP_L_XM	0x05
#define OUT_TEMP_H_XM	0x06

// Acceleration
#define ACC_ADDR       0x1D
#define CTRL_REG0_XM   0x1F
#define CTRL_REG1_XM   0x20
#define CTRL_REG2_XM   0x21
#define WHO_AM_I_A     0x0F
#define ACC_ID         0x49
#define ACC_X_EN	   0x02
#define ACC_Y_EN	   0x01
#define ACC_Z_EN	   0x04
#define ACC_BDU		   0x08
#define OUT_X_L_A	   0x28  // Base of 6 byte XYZ
#define ACC_FACTOR	   LSM9DS0_ACCEL_MG_LSB_16G * SENSORS_GRAVITY_EARTH / 1000

// Linear Acceleration: mg per LSB
#define LSM9DS0_ACCEL_MG_LSB_2G (0.061F)
#define LSM9DS0_ACCEL_MG_LSB_4G (0.122F)
#define LSM9DS0_ACCEL_MG_LSB_6G (0.183F)
#define LSM9DS0_ACCEL_MG_LSB_8G (0.244F)
#define LSM9DS0_ACCEL_MG_LSB_16G (0.732F) // Is this right? Was expecting 0.488F

#define LSM9DS0_TIMEOUT 500         // in ms
#define LSM9DS0_MASK    0x80
/* Gyroscope offsets */
#define AUTO_GYRO_CALIB 0
#define GYRO_N          100         // Calculate gyroscope offset
#define GYRO_OFFSET_X   72.38
#define GYRO_OFFSET_Y   381.63
#define GYRO_OFFSET_Z   409.60

/* LP Filter */
#define A_GYRO 0.7    // Coefficient < 1.0
#define A_MGN  0.7    // Coefficient < 1.0
#define A_XM   0.7    // Coefficient < 1.0

#define DEG2RAD 0.017453293
#define RAD(x) ((x)*DEG2RAD)

typedef enum {
	DEVICE_ERROR = 0, DEVICE_NORMAL, DEVICE_ENABLE, DEVICE_DISABLE
} lsm9ds0_sensor_status;

typedef struct {
	int16_t gyr_raw[3];
	float gyr[3];
	float gyr_prev[3];
	float gyr_f[3];
	float calib_gyr[3];
	lsm9ds0_sensor_status gyr_status;

	int16_t xm_raw[3];
	float xm_norm;
	float xm[3];
	float xm_prev[3];
	float xm_f[3];
	lsm9ds0_sensor_status xm_status;

	int16_t temp_raw;
	float temp;
	lsm9ds0_sensor_status temp_status;

	int16_t acc_raw[3];
	float acc[3];
	lsm9ds0_sensor_status acc_status;

} lsm9ds0_sensor;



typedef enum {
	LSM9DS0_ACCELRANGE_2G = (0b000 << 3),
	LSM9DS0_ACCELRANGE_4G = (0b001 << 3),
	LSM9DS0_ACCELRANGE_6G = (0b010 << 3),
	LSM9DS0_ACCELRANGE_8G = (0b011 << 3),
	LSM9DS0_ACCELRANGE_16G = (0b100 << 3)
} lsm9ds0AccelRange_t;

typedef enum {
	LSM9DS0_ACCELDATARATE_POWERDOWN = (0b0000 << 4),
	LSM9DS0_ACCELDATARATE_3_125HZ = (0b0001 << 4),
	LSM9DS0_ACCELDATARATE_6_25HZ = (0b0010 << 4),
	LSM9DS0_ACCELDATARATE_12_5HZ = (0b0011 << 4),
	LSM9DS0_ACCELDATARATE_25HZ = (0b0100 << 4),
	LSM9DS0_ACCELDATARATE_50HZ = (0b0101 << 4),
	LSM9DS0_ACCELDATARATE_100HZ = (0b0110 << 4),
	LSM9DS0_ACCELDATARATE_200HZ = (0b0111 << 4),
	LSM9DS0_ACCELDATARATE_400HZ = (0b1000 << 4),
	LSM9DS0_ACCELDATARATE_800HZ = (0b1001 << 4),
	LSM9DS0_ACCELDATARATE_1600HZ = (0b1010 << 4)
} lm9ds0AccelDataRate_t;

typedef enum accel_abw_x
{
	A_ABW_773,		// 773 Hz (0x0)
	A_ABW_194,		// 194 Hz (0x1)
	A_ABW_362,		// 362 Hz (0x2)
	A_ABW_50,		//  50 Hz (0x3)
} lm9ds0AccelABW;

typedef enum accel_ast_x
{
	A_AST_NORMAL,
	A_AST_POSITIVE,
	A_AST_NEGATIVE,
	A_AST_NA,
} lm9ds0AccelAST;


lsm9ds0_sensor_status init_lsm9ds0_gyro(lsm9ds0_sensor *sensors);
void calib_lsm9ds0_gyro(lsm9ds0_sensor *sensors);
lsm9ds0_sensor_status update_lsm9ds0_gyro(lsm9ds0_sensor *sensors);
lsm9ds0_sensor_status init_lsm9ds0_xm(lsm9ds0_sensor *sensors);
lsm9ds0_sensor_status update_lsm9ds0_xm(lsm9ds0_sensor *sensors);
lsm9ds0_sensor_status init_lsm9ds0_acc(lsm9ds0_sensor *sensors);
lsm9ds0_sensor_status update_lsm9ds0_acc(lsm9ds0_sensor *sensors);
lsm9ds0_sensor_status init_lsm9ds0_temp(lsm9ds0_sensor *sensors);
lsm9ds0_sensor_status update_lsm9ds0_temp(lsm9ds0_sensor *sensors);
#endif /* LSM9DS0_H_ */
