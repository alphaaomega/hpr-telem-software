/*
 * hpr_ring_buffer.h
 *
 *  Created on: Apr 10, 2017
 *      Author: ariolis
 */

#ifndef HPR_RING_BUFFER_H_
#define HPR_RING_BUFFER_H_

#include "hpr_datatypes.h"

#define RINGBUFFER_LENGTH 10 // This is the length of the ring buffer. When the ring buffer is full
                             // all the buffered data are saved to the sd memory card, and the gps
                             // logged values are transmitted over rfm.

typedef struct
{

	HPR_DataStoreTypeDef  data_log[RINGBUFFER_LENGTH]; /*Ring buffer data structure. Holds all the
	                                                     logged data */

	uint8_t index;  /*index of ring buffer. used to manage reading/writting from/to
	                 the ring buffer data structure..*/


}HPR_RingBufferTypeDef;




void HPR_RingBufferInit(HPR_RingBufferTypeDef *ring_buffer);

void HPR_RingBufferAddEntry(HPR_RingBufferTypeDef *ring_buffer, IMU_DataStoreTypeDef *imu_ring_buffer, GPS_DataStoreTypeDef *gps_data, BME_DataStoreTypeDef *bme_data, uint8_t battery_voltage_now , uint32_t timeticks_now);

uint8_t HPR_RingBufferGetIndex(HPR_RingBufferTypeDef *ring_buffer);

uint8_t HPR_RingBufferIsFull(HPR_RingBufferTypeDef *ring_buffer);/*return non zero if full, zero otherwise*/

void HPR_RingBufferSaveToSd(HPR_RingBufferTypeDef *ring_buffer);

void HPR_RingBufferSaveToFlash(HPR_RingBufferTypeDef *ring_buffer);

#endif /* HPR_RING_BUFFER_H_ */
