/*
 * hpr_lsm9ds0_hal.h
 *
 *  Created on: Apr 16, 2017
 *      Author: ariolis
 */

#ifndef HPR_LSM9DS0_HAL_H_
#define HPR_LSM9DS0_HAL_H_



#include "stm32l1xx_hal.h"
#include "stm32l1xx_hal_i2c.h"
#include "lsm9ds0.h"
#include "hpr_datatypes.h"


extern I2C_HandleTypeDef hi2c1;

lsm9ds0_sensor_status init_hpr_lsm9ds0( void);

void update_hpr_lsm9ds0_xm(IMU_DataStoreTypeDef *imu_data_bufer);
void update_hpr_lsm9ds0_gyro(IMU_DataStoreTypeDef *imu_data_bufer);
void update_hpr_lsm9ds0_temp(IMU_DataStoreTypeDef *imu_data_bufer);
void update_hpr_lsm9ds0_acc(IMU_DataStoreTypeDef *imu_data_bufer);

#endif /* HPR_LSM9DS0_HAL_H_ */
