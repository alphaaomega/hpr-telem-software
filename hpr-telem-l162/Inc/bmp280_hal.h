/*
 * bmp280_support.h
 *
 *  Created on: Apr 16, 2017
 *      Author: drid
 */

#ifndef BMP280_DRIVER_BMP280_SUPPORT_H_
#define BMP280_DRIVER_BMP280_SUPPORT_H_
#include <bmp280.h>

s32 bmp280_data_read(u32 *pressure, s32 *temperature);

#endif /* BMP280_DRIVER_BMP280_SUPPORT_H_ */
